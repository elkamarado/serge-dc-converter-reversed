# Unknown DC-DC Converter restored by a pair of photos

Doesn't contain the paired control board shown on a photo.


## Source

Top
![Alt text](images/image-2.png)

Bottom
![Alt text](images/image-3.png)


## Schematics

![Alt text](images/image-1.png)


## PCB

Front copper
![Alt text](images/image-4.png)

Back copper
![Alt text](images/image-5.png)


## 3D

Top
![Top](images/image-10.png)

Bottom
![Bottom](images/image-11.png)

Over
![Over](images/image-9.png)
